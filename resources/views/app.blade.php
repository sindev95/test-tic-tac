<!DOCTYPE html>
<html class="h-full bg-gray-100 dark">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/svg+xml" href="/favicon.svg">
    @routes
    @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
    @inertiaHead
</head>

<body class="dark:bg-slate-900 bg-gray-100 h-screen items-center">
@inertia
</body>

</html>
