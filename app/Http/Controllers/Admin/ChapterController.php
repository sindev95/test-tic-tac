<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Chapter;
use App\Models\Domain;
use Illuminate\Http\Request;

class ChapterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return inertia('Admin/Chapters/Table', ['chapters' => Chapter::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia('Admin/Chapters/Create', ['domains' => Domain::get()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if (Chapter::create($request->validate(['name' => 'required', 'domain_id' => 'required']))) {
            return redirect(route('admin.chapters.index'))->with('success', 'Chapter was saved successfully');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Chapter $chapter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Chapter $chapter)
    {
        return inertia('Admin/Chapters/Edit', ['chapter' => $chapter, 'domains' => Domain::get()]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Chapter $chapter)
    {
        if($chapter->update($request->validate(['name' => 'required', 'domain_id' => 'required']))){
            return back()->with('success', 'Chapter was saved successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Chapter $chapter)
    {
        if ($chapter->delete()) {
            return back()->with('success', 'Chapter deleted successfully');
        }
    }
}
