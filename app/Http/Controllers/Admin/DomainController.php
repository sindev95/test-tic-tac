<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Domain;
use Illuminate\Http\Request;

class DomainController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return inertia('Admin/Domains/Table', ['domains' => Domain::paginate(5)]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia('Admin/Domains/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if (Domain::create($request->validate(['name' => 'required|max:255']))) {
            return redirect(route('admin.domains.index'))->with('success', 'Successfully created new domain');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Domain $domain)
    {
        return inertia('Admin/Domains/Edit', ['domain' => $domain]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Domain $domain)
    {
        if ($domain->update($request->validate(['name' => 'required|max:255']))) {
            return redirect(route('admin.domains.index'))->with('success', 'Successully updated domain');
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Domain $domain)
    {
        if ($domain->delete()) {
            return back()->with('success', 'Domain deleted successfully');
        }
    }
}
