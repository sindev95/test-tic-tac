<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Chapter;
use App\Models\Domain;
use App\Models\Exam;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return inertia('Admin/Exams/Table', ['exams' => Exam::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia('Admin/Exams/Create', ['domains' => Domain::get()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if (Exam::create($request->validate([
            'name' => 'required|string',
            'domain_id' => 'required|exists:domains,id',
            'exam_time' => 'required|integer|min:1',
            'chapters' => 'required|array',
        ]))) {
            return redirect(route('admin.exams.index'))->with('success', 'Exam was saved successfully');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Exam $exam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Exam $exam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Exam $exam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Exam $exam)
    {
        //
    }

    public function getChapters(Domain $domain)
    {
        return response()->json(['chapters' => Chapter::where('domain_id', $domain->id)->get()]);
    }
}
