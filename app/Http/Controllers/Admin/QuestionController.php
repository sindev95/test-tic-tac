<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Chapter;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return inertia('Admin/Questions/Table', ['questions' => Question::paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia('Admin/Questions/Create', ['chapters' => Chapter::get()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if (Question::create($request->validate([
            'text' => 'required|string',
            'chapter_id' => 'required',
            'answers' => 'required|array|min:4',
            'answers.*.text' => 'required|string',
            'answers.*.is_correct' => 'required|boolean',
        ]))) {
            return redirect(route('admin.questions.index'))->with('success', 'Successfully inserted question');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Question $question)
    {
        return inertia('Admin/Questions/Edit', ['question' => $question, 'chapters' => Chapter::get()]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Question $question)
    {
        if ($question->update($request->validate([
            'text' => 'required|string',
            'chapter_id' => 'required',
            'answers' => 'required|array|min:4',
            'answers.*.text' => 'required|string',
            'answers.*.is_correct' => 'required|boolean',
        ]))) {
            return redirect(route('admin.questions.index'))->with('success', 'Successfully updated question');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Question $question)
    {
        if($question->delete()){
            return back()->with('success','Successfully deleted question');
        }
    }
}
