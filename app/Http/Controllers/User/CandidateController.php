<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Chapter;
use App\Models\Exam;
use App\Models\Question;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    public function index()
    {
        return inertia('Candidate/Exams', ['exams' => Exam::paginate(1)]);
    }

    public function takeExam(Exam $exam)
    {
        $questions = [];

        foreach ($exam->chapters as $chapterId) {
            $chapter = Chapter::find($chapterId);
            $chapterQuestions = $chapter->questions()->inRandomOrder()->take(4)->with('chapter:id,name')->get();
            $chapterQuestions->transform(function ($question) {

                $countCorrectAnswers = collect($question['answers'])->filter(function ($answer) {
                    return $answer->is_correct;
                })->count();
                $question['answers'] = collect($question['answers'])->pluck('text');
                $question['has_multiple_choices'] = $countCorrectAnswers > 1;
                return $question;
            });
            $questions = array_merge($questions, $chapterQuestions->toArray());
        }

        return inertia('Candidate/PassExam', ['questions' => $questions,'exam_time' => $exam->exam_time]);
    }


    public function verifyExam(Request $request){
        $totalScore = 0;
        foreach ($request->questionAnswers as $CandidateAnswer){
            $selectedAnswers = $CandidateAnswer['answers'];
            $question = Question::findOrFail($CandidateAnswer['question_id']);
            $correctAnswers = collect($question->answers)->filter(function ($answer) {
                return $answer->is_correct;
            })->pluck('text')->toArray();

            sort($selectedAnswers);
            sort($correctAnswers);

            if ($selectedAnswers === $correctAnswers) {
                $totalScore++;
            }
        }
        return response()->json(['score' => $totalScore]);
    }
}
