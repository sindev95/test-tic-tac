<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'domain_id', 'exam_time', 'chapters'];

    protected $casts = [
        'chapters' => 'array',
    ];

    protected function Chapters(): Attribute
    {
        return Attribute::make(
            get: fn($value) => json_decode($value),
            set: fn($value) => $this->attributes['chapters'] = json_encode($value)
        );
    }
}
