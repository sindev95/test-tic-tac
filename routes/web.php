<?php

use App\Http\Controllers\Admin\ChapterController;
use App\Http\Controllers\Admin\DomainController;
use App\Http\Controllers\Admin\ExamController;
use App\Http\Controllers\Admin\QuestionController;
use App\Http\Controllers\User\CandidateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect(route('candidate.index'));
});


Route::group([],function (){
   Route::group([
       'prefix'=>'admin',
       'as'  =>'admin.'
   ],function (){
       Route::resource('domains', DomainController::class);
       Route::resource('chapters', ChapterController::class);
       Route::resource('questions', QuestionController::class);
       Route::resource('exams', ExamController::class);
       Route::get('getChapters/{domain}',[ExamController::class,'getChapters'])->name('getChapters');
   });

    Route::group([
        'prefix'=>'candidate',
        'as'  =>'candidate.'
    ],function (){
        Route::get('/', [CandidateController::class,'index'])->name('index');
        Route::get('/takeExam/{exam}', [CandidateController::class,'takeExam'])->name('takeExam');
        Route::get('/verifyExam', [CandidateController::class,'verifyExam'])->name('verifyExam');
    });
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
